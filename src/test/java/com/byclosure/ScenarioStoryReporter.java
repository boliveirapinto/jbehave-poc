package com.byclosure;

import org.jbehave.core.model.Story;
import org.jbehave.core.reporters.NullStoryReporter;

/**
 *
 */
public class ScenarioStoryReporter extends NullStoryReporter {

    private String scenarioTitle;
    private String storyName;

    @Override
    public void beforeScenario(String title) {
        scenarioTitle = title;
    }

    @Override
    public void beforeStory(Story story, boolean givenStory) {
        this.storyName = story.getName();
    }

    public String getScenarioTitle() {
        return scenarioTitle;
    }
    public String getStoryName() {
        return storyName;
    }
}
